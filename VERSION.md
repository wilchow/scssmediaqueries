# scssMediaQueries

## February 21, 2018
* ⬆ Updated `normalize.css` to v8.0
* ⬆ Updated `_mixins`

## August 8, 2017
* 🔥 Removed [Lato](https://fonts.google.com/specimen/Lato) as the default sans serif font
* ✨ Added [Open Sans](https://fonts.google.com/specimen/Open+Sans) as the default sans serif font
* ✨ Added [Droid Serif](https://fonts.google.com/specimen/Droid+Serif) as the default serif font
* ⬆ Updated `normalize.css` to v7

## April 9, 2015
### v0.1.0
* Initial commit
* Created the base files